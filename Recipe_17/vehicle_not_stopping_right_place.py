"""Raise an alert when a Vehicle is not stopping at the right place at the Stop bar (Recipe #17)."""
# Standard imports
from typing import Set

# Third party import
import numpy as np
import osef

SHIFT_IP = "192.168.2.2"

VEHICLE_NAMES = [
    "TWO_WHEELER",
    "CAR",
    "VAN",
    "TRUCK",
]
ZONE_NAME = "stop_bar"
INVALID_STOP_ZONE = "invalid_stop"
SPEED_LIMIT = 0.2  # [km/h]

for frame_index, frame_dict in enumerate(osef.parser.parse(f"tcp://{SHIFT_IP}")):
    # Extract data from scan frame
    scan_frame = frame_dict["timestamped_data"]["scan_frame"]
    tracked_objects = scan_frame["tracked_objects"]
    object_ids = tracked_objects.get("object_id_32_bits").tolist()
    class_names = tracked_objects["class_id_array"]["class_name"]
    zone_definitions = scan_frame["zones_def"]
    zone_bindings = scan_frame.get("zones_objects_binding_32_bits")

    # Check if a vehicle is stopped in the defined zone
    stopping_vehicles: Set[int] = set()
    for object_id, zone_index in zone_bindings:
        object_index = object_ids.index(object_id)
        if class_names[object_index] not in VEHICLE_NAMES:
            continue

        zone_name = zone_definitions[zone_index]["zone"]["zone_name"]
        if zone_name not in ZONE_NAME:
            continue

        speed_vector = tracked_objects["speed_vectors"].tolist()[object_index]
        speed = np.linalg.norm(speed_vector) * 3600 / 1000  # [km/h]
        if speed > SPEED_LIMIT:
            continue

        # Check vehicle did not stop in the invalid zone (further than the stop bar)
        for veh_id, invalid_id in zone_bindings:
            if (
                veh_id != object_id
                or zone_definitions[invalid_id]["zone"]["zone_name"]
                not in INVALID_STOP_ZONE
            ):
                continue
            stopping_vehicles.add(veh_id)

    print(
        f"ALERT: Vehicles {stopping_vehicles} not stopping at the right place at the stop bar"
    )
