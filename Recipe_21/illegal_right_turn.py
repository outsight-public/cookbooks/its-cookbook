"""Raise an alert when a vehicle makes an illegal right-turn (Recipe #17)."""
# Standard imports
import datetime
from typing import Set

# Third party imports
import osef

SHIFT_IP = "192.168.2.2"

VEHICLE_NAMES = [
    "TWO_WHEELER",
    "CAR",
    "VAN",
    "TRUCK",
]
MAIN_ROAD_ZONE = "mainroad"
ILLEGAL_RIGHT_ZONE = "illegal_right"
vehicles_mainroad: Set[int] = set()

for frame_index, frame_dict in enumerate(osef.parser.parse(f"tcp://{SHIFT_IP}")):
    # Extract data from frame
    scan_frame = frame_dict["timestamped_data"]["scan_frame"]
    timestamp = frame_dict["timestamped_data"]["timestamp_microsecond"]
    tracked_objects = scan_frame["tracked_objects"]
    object_ids = tracked_objects.get("object_id_32_bits").tolist()
    zone_definitions = scan_frame["zones_def"]
    zone_bindings = scan_frame.get("zones_objects_binding_32_bits")
    class_names = tracked_objects["class_id_array"]["class_name"]

    # Check for vehicles on defined zones.
    for object_id, zone_index in zone_bindings:
        object_index = object_ids.index(object_id)
        object_class = class_names[object_index]
        zone_name = zone_definitions[zone_index]["zone"]["zone_name"]
        if object_class not in VEHICLE_NAMES:
            continue
        if zone_name in MAIN_ROAD_ZONE:
            vehicles_mainroad.add(object_id)
        elif zone_name in ILLEGAL_RIGHT_ZONE and object_id in vehicles_mainroad:
            time = datetime.datetime.fromtimestamp(timestamp).strftime(
                "%d/%m/%Y %H:%M:%S"
            )
            vehicles_mainroad.remove(object_id)
            print(
                f"ALERT: At {time}: {object_class} {object_id} making an illegal right turn"
            )
        elif object_id in vehicles_mainroad:
            vehicles_mainroad.remove(object_id)
