"""Raise an alert if a car parks for more than a selected time in a quick drop-off zone"""
# Standard imports
from typing import Dict, Set

# Third party import
import osef

SHIFT_IP = "192.168.2.2"
TIME_LIMIT = 5  # [minutes]

parked_cars: Dict[str, Dict[str, int]] = {}

for frame_index, frame_dict in enumerate(osef.parser.parse(f"tcp://{SHIFT_IP}")):
    # Extract data from scan frame.
    timestamp = frame_dict["timestamped_data"]["timestamp_microsecond"]
    scan_frame = frame_dict["timestamped_data"]["scan_frame"]
    tracked_objects = scan_frame["tracked_objects"]
    object_ids = tracked_objects.get("object_id_32_bits").tolist()
    zone_definitions = scan_frame["zones_def"]
    zone_bindings = scan_frame.get("zones_objects_binding_32_bits")
    class_names = tracked_objects["class_id_array"]["class_name"]

    # Check car in the drop-off zone.
    current_cars_in_dropoff: Set[int] = set()
    for object_id, zone_index in zone_bindings:
        object_index = object_ids.index(object_id)
        zone_name = zone_definitions[zone_index]["zone"]["zone_name"]

        # Update parking time.
        if class_names[object_index] == "CAR" and "dropoff" in zone_name:
            current_cars_in_dropoff.add(object_id)
            if object_id in parked_cars:
                parked_cars[object_id]["duration"] = (
                    timestamp - parked_cars[object_id]["arrival"]
                )
            else:
                parked_cars[object_id] = {"arrival": timestamp, "duration": 0}

    # Remove cars who left the dropoff zone.
    cars_to_remove: Set[str] = set()
    for car_id, car in parked_cars.items():
        if car["duration"] > TIME_LIMIT * 60:
            print(f"ALERT: Car {car_id} parks for more than {TIME_LIMIT} mins")
        if int(car_id) not in current_cars_in_dropoff:
            cars_to_remove.add(car_id)

    for car_id in cars_to_remove:
        del parked_cars[car_id]
