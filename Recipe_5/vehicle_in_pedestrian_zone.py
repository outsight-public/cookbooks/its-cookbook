"""Raise an alert if a vehicle enters a pedestrian-only zone (#Recipe 5)"""
# Third party imports
import osef

SHIFT_IP = "192.168.2.2"

OBJECT_CLASS = "PEDESTRIAN"
ZONE_NAME = "pedestrianzone"

VEHICLE_NAMES = ["TWO_WHEELER", "CAR", "VAN", "TRUCK"]

for frame_index, frame_dict in enumerate(osef.parser.parse(f"tcp://{SHIFT_IP}")):
    # Extract data from scan frame.
    scan_frame = frame_dict["timestamped_data"]["scan_frame"]
    tracked_objects = scan_frame["tracked_objects"]
    object_ids = tracked_objects.get("object_id_32_bits").tolist()
    zone_definitions = scan_frame["zones_def"]
    zone_bindings = scan_frame.get("zones_objects_binding_32_bits")
    class_names = tracked_objects["class_id_array"]["class_name"]

    vehicles_in_zone = set()

    # Check vehicle in the pedestrian-only zone.
    for object_id, zone_index in zone_bindings:
        object_index = object_ids.index(object_id)
        zone_name = zone_definitions[zone_index]["zone"]["zone_name"]

        # Update staying time.
        if class_names[object_index] in VEHICLE_NAMES and ZONE_NAME in zone_name:
            vehicles_in_zone.add(object_id)

    # Print alert.
    if len(vehicles_in_zone) > 0:
        print(f"ALERT: Vehicle(s) {vehicles_in_zone} is/are in a pedestrian-only zone.")
