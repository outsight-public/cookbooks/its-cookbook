"""Provide the % of Cars/Vans, Truck or two-wheelers that exceed the authorized speed (Recipe #14)"""
# Standard imports
from typing import Dict, Set

# Third party imports
import osef
import numpy as np

SHIFT_IP = "192.168.2.2"

VEHICLE_NAMES = ["TWO_WHEELER", "CAR", "VAN", "TRUCK"]

SPEED_LIMIT = 25  # [km/h]

vehicles: Dict[str, Dict[str, Set[int]]] = {}
for vehicle in VEHICLE_NAMES:
    vehicles[vehicle] = {"total": set(), "overspeed": set()}
vehicle_groups = [["CAR", "VAN"], ["TRUCK"], ["TWO_WHEELER"]]

for frame_index, frame_dict in enumerate(osef.parser.parse(f"tcp://{SHIFT_IP}")):
    # extract data from scan frame
    tracked_objects = frame_dict["timestamped_data"]["scan_frame"]["tracked_objects"]
    object_ids = tracked_objects.get("object_id_32_bits")
    class_names = tracked_objects["class_id_array"]["class_name"]

    for object_index, class_name, speed_vector in zip(
        object_ids,
        class_names,
        tracked_objects["speed_vectors"].tolist(),
    ):
        # skip if the object is not classified as a vehicle
        if class_name not in VEHICLE_NAMES:
            continue

        vehicles[class_name]["total"].add(object_index)
        speed = np.linalg.norm(speed_vector) * 3600 / 1000  # [km/h]
        if speed > SPEED_LIMIT:
            vehicles[class_name]["overspeed"].add(object_index)

    # Display current statistics
    print(
        f"INFO: Statistics on overspeed tracked vehicles (Limit speed {SPEED_LIMIT}km/h)"
    )
    for group in vehicle_groups:
        number: int = 0
        overspeed: int = 0
        for veh_type in group:
            number += len(vehicles[veh_type]["total"])
            overspeed += len(vehicles[veh_type]["overspeed"])
        percent = "{:.2f}".format(overspeed / number * 100) if (number > 0) else 0

        print(f"* {percent}% of {group} exceed the authorized speed")
