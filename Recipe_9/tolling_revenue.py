"""Provide the tolling revenue from each type of vehicle (Recipe 9)"""
# Standard imports
from typing import Dict, Set

# Third party imports
import osef

SHIFT_IP = "192.168.2.2"

VEHICLE_TOLL_PRICE = {
    "TWO_WHEELER": 10,
    "CAR": 15,
    "VAN": 18.50,
    "TRUCK": 25,
}
TOLL_GATE = "tollgate"

tolling_vehicles: Dict[str, Set[int]] = {}

for vehicle in VEHICLE_TOLL_PRICE:
    tolling_vehicles[vehicle] = set()

for frame_index, frame_dict in enumerate(osef.parser.parse(f"tcp://{SHIFT_IP}")):
    # Extract data from scan frame
    scan_frame = frame_dict["timestamped_data"]["scan_frame"]
    tracked_objects = scan_frame["tracked_objects"]
    object_ids = tracked_objects.get("object_id_32_bits").tolist()
    class_names = tracked_objects["class_id_array"]["class_name"]
    zone_definitions = scan_frame["zones_def"]
    zone_bindings = scan_frame.get("zones_objects_binding_32_bits")

    # Add vehicles to the vehicle revenues if they're going through the toll gate.
    for object_id, zone_index in zone_bindings:
        object_index = object_ids.index(object_id)
        zone_name = zone_definitions[zone_index]["zone"]["zone_name"]
        if (
            class_names[object_index] not in VEHICLE_TOLL_PRICE
            or TOLL_GATE not in zone_name
        ):
            continue

        tolling_vehicles[class_names[object_index]].add(object_id)

    print("INFO: Tolling revenue:")
    total_revenue: float = 0
    for vehicle_class, ids in tolling_vehicles.items():
        key_revenue = len(ids) * VEHICLE_TOLL_PRICE[vehicle_class]
        total_revenue += key_revenue
        print(f"* {key_revenue}€ coming from {vehicle_class}")

    print(f"** For a total of {total_revenue}€ tolling revenue")
