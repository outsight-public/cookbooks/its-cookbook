"""Provide statistics about the vehicle length (Recipe 10)"""
# Standard imports
from typing import List

# Third party imports
import osef

SHIFT_IP = "192.168.2.2"

vehicles: List[int] = []
vehicles_length: List[int] = []

for frame_index, frame_dict in enumerate(osef.parser.parse(f"tcp://{SHIFT_IP}")):
    # Extract data from scan frame
    tracked_objects = frame_dict["timestamped_data"]["scan_frame"]["tracked_objects"]
    object_ids = tracked_objects.get("object_id_32_bits")
    class_names = tracked_objects["class_id_array"]["class_name"]

    for object_index, class_name, bbox in zip(
        object_ids,
        class_names,
        tracked_objects["bbox_sizes"].tolist(),
    ):
        # skip if the object is not classified as a vehicle
        if class_name != "TRUCK":
            continue

        if object_index not in vehicles:
            vehicles.append(object_index)
            vehicles_length.append(bbox[1])

    print(f"INFO: Statistics on {len(vehicles)} Trucks")
    print(f"* Maximal length: {max(vehicles_length)}m")
    print(f"* Minimal length: {min(vehicles_length)}m")
    print(f"* Average length: {sum(vehicles_length) / len(vehicles)}m")
