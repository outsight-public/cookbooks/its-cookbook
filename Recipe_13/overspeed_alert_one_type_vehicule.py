"""Raise an alert with position and speed of the vehicle,
when a vehicle exceeds speed (Recipe #13)."""
# STandard imports
from typing import Set

# Third party imports
import osef
import numpy as np

SHIFT_IP = "192.168.2.2"

VEHICLE_NAMES = [
    "TWO_WHEELER",
]

SPEED_LIMIT = 30  # [km/h]

speeding_vehicles: Set[int] = set()

for frame_index, frame_dict in enumerate(osef.parser.parse(f"tcp://{SHIFT_IP}")):
    # extract data from scan frame
    tracked_objects = frame_dict["timestamped_data"]["scan_frame"]["tracked_objects"]
    object_ids = tracked_objects["object_id"]
    class_names = tracked_objects["class_id_array"]["class_name"]

    for object_index, class_name, speed_vector, pose in zip(
        object_ids,
        class_names,
        tracked_objects["speed_vectors"].tolist(),
        tracked_objects["pose_array"],
    ):
        # skip if the object is not classified as a Two-wheeler
        if class_name not in VEHICLE_NAMES:
            continue

        position = pose["translation"]
        speed = np.linalg.norm(speed_vector) * 3600 / 1000  # [km/h]
        if speed > SPEED_LIMIT:
            vehicle_id = object_ids[object_index]
            speeding_vehicles.add(vehicle_id)
            print(
                f"ALARM: Two-wheeler {vehicle_id} caught speeding "
                f"at position {position[:2]} "
                f"({SPEED_LIMIT} km/h < {round(speed, 2)} km/h) "
            )

print(
    f"A total of {len(speeding_vehicles)} Two-wheelers have been caught over speeding"
)
