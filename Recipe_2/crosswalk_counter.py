"""Counter for people on crosswalk (Recipe #2)."""
# Third party imports
import osef

SHIFT_IP = "192.168.2.2"

pedestrian_on_crosswalk_ids = []
for frame_index, frame_dict in enumerate(osef.parser.parse(f"tcp://{SHIFT_IP}")):
    # extract data from frame
    scan_frame = frame_dict["timestamped_data"]["scan_frame"]
    tracked_objects = scan_frame["tracked_objects"]
    object_ids = tracked_objects.get("object_id_32_bits").tolist()
    zone_definitions = scan_frame["zones_def"]
    zone_bindings = scan_frame.get("zones_objects_binding_32_bits")
    class_names = tracked_objects["class_id_array"]["class_name"]

    # add pedestrian object id if they are in the crosswalk zone
    for object_id, zone_index in zone_bindings:
        object_index = object_ids.index(object_id)
        object_class = class_names[object_index]
        zone_name = zone_definitions[zone_index]["zone"]["zone_name"]
        if (
            object_class == "PERSON"
            and "crosswalk" in zone_name
            and object_id not in pedestrian_on_crosswalk_ids
        ):
            pedestrian_on_crosswalk_ids.append(object_id)

print(
    f"{len(pedestrian_on_crosswalk_ids)} pedestrians used the crosswalk to cross the road."
)
