"""Raise an alert if a pedestrian stays more than a selected time in a crosswalk zone (Recipe 4)"""
# Standard imports
from typing import Dict

# Third party imports
import osef

SHIFT_IP = "192.168.2.2"
TIME_LIMIT = 2  # [minutes]
OBJECT_CLASS = "PEDESTRIAN"
ZONE_NAME = "crosswalk"

pedestrians: Dict[int, Dict[str, int]] = {}

for frame_index, frame_dict in enumerate(osef.parser.parse(f"tcp://{SHIFT_IP}")):
    # Extract data from scan frame.
    timestamp = frame_dict["timestamped_data"]["timestamp_microsecond"]
    scan_frame = frame_dict["timestamped_data"]["scan_frame"]
    tracked_objects = scan_frame["tracked_objects"]
    object_ids = tracked_objects.get("object_id_32_bits").tolist()
    zone_definitions = scan_frame["zones_def"]
    zone_bindings = scan_frame.get("zones_objects_binding_32_bits")
    class_names = tracked_objects["class_id_array"]["class_name"]

    # Check pedestrian in the crosswalk zone.
    ped_in_crosswalk = set()
    for object_id, zone_index in zone_bindings:
        object_index = object_ids.index(object_id)
        zone_name = zone_definitions[zone_index]["zone"]["zone_name"]

        # Update staying time.
        if class_names[object_index] == OBJECT_CLASS and ZONE_NAME in zone_name:
            ped_in_crosswalk.add(object_id)
            if object_id in pedestrians:
                pedestrians[object_id]["duration"] = (
                    timestamp - pedestrians[object_id]["arrival"]
                )
            else:
                pedestrians[object_id] = {"arrival": timestamp, "duration": 0}

    # Remove pedestrians who left the crosswalk zone.
    pedestrians_to_remove = set()
    for ped_id, car in pedestrians.items():
        if car["duration"] > TIME_LIMIT * 60:
            print(
                f"ALERT: Pedestrian {ped_id} stayed in crosswalk for more than {TIME_LIMIT} mins"
            )

        if ped_id not in ped_in_crosswalk:
            pedestrians_to_remove.add(ped_id)

    for ped_id in pedestrians_to_remove:
        del pedestrians[ped_id]
