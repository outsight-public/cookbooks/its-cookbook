"""Raise an alert when at least one pedestrian is on crosswalk (Recipe #1)."""
import osef

ALB_IP = "192.168.2.2"

for frame_index, frame_dict in enumerate(osef.parser.parse(f"tcp://{ALB_IP}")):
    # Extract data from frame
    scan_frame = frame_dict["timestamped_data"]["scan_frame"]
    tracked_objects = scan_frame["tracked_objects"]
    object_ids = tracked_objects.get("object_id_32_bits").tolist()
    zone_definitions = scan_frame["zones_def"]
    zone_bindings = scan_frame.get("zones_objects_binding_32_bits")
    class_names = tracked_objects["class_id_array"]["class_name"]

    pedestrians = []

    # Check if a pedestrian is on the crosswalk
    for object_id, zone_index in zone_bindings:
        object_index = object_ids.index(object_id)
        object_class = class_names[object_index]
        zone_name = zone_definitions[zone_index]["zone"]["zone_name"]
        if object_class == "PERSON" and "crosswalk" in zone_name:
            pedestrians.append(object_id)

    if len(pedestrians) > 0:
        print(f"ALERT: Pedestrian(s) {pedestrians} is/are in the crosswalk")
