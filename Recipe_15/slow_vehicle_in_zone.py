"""Raise an alert when a vehicle travels below the minimum speed,
on a defined lane for an amount of time (Recipe #15)"""
# Standard imports
from typing import Dict

# Third party imports
import osef
import numpy as np

SHIFT_IP = "192.168.2.2"

VEHICLE_NAMES = ["TWO_WHEELER", "CAR", "VAN", "TRUCK"]

MIN_SPEED_LIMIT = 70  # [km/h]
SLOW_ZONE = "left_lane"
TIME_LIMIT = 1  # minutes

slow_speeding_vehicles: Dict[str, Dict[str, int]] = {}

for frame_index, frame_dict in enumerate(osef.parser.parse(f"tcp://{SHIFT_IP}")):
    # extract data from scan frame
    timestamp = frame_dict["timestamped_data"]["timestamp_microsecond"]
    scan_frame = frame_dict["timestamped_data"]["scan_frame"]
    tracked_objects = scan_frame["tracked_objects"]
    object_ids = tracked_objects.get("object_id_32_bits").tolist()
    zone_definitions = scan_frame["zones_def"]
    zone_bindings = scan_frame.get("zones_objects_binding_32_bits")
    class_names = tracked_objects["class_id_array"]["class_name"]

    slow_vehicles = set()
    for object_id, zone_index in zone_bindings:
        object_index = object_ids.index(object_id)
        zone_name = zone_definitions[zone_index]["zone"]["zone_name"]
        if class_names[object_index] not in VEHICLE_NAMES or zone_name not in SLOW_ZONE:
            continue

        speed_vector = tracked_objects["speed_vectors"].tolist()[object_index]
        speed = np.linalg.norm(speed_vector) * 3600 / 1000  # [km/h]

        if speed > MIN_SPEED_LIMIT:
            continue

        slow_vehicles.add(object_id)
        if object_id in slow_speeding_vehicles:
            slow_speeding_vehicles[object_id]["duration"] = (
                timestamp - slow_speeding_vehicles[object_id]["start"]
            )
        else:
            slow_speeding_vehicles[object_id] = {"start": timestamp, "duration": 0}

    # Raise alert if necessary and updates the slow vehicles.
    vehicles_to_remove = set()
    for veh_id, veh in slow_speeding_vehicles.items():
        if veh["duration"] > TIME_LIMIT * 60:
            object_index = object_ids.index(veh_id)
            print(
                f"ALERT: At {timestamp}:"
                f"{class_names[object_index]} {veh_id} travels below the minimum speed"
                f"({MIN_SPEED_LIMIT}km/h) for more than {TIME_LIMIT}mins"
            )
        if veh_id not in slow_vehicles:
            vehicles_to_remove.add(veh_id)

    for veh_id in vehicles_to_remove:
        del slow_speeding_vehicles[veh_id]
