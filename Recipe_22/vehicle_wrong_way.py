"""Raise an alert when a vehicle is going in the wrong direction (Recipe #22)."""
# Standard imports
import datetime
from typing import Any, List

# Third party imports
import numpy as np
import osef

SHIFT_IP = "192.168.2.2"

VEHICLE_NAMES = [
    "TWO_WHEELER",
    "CAR",
    "VAN",
    "TRUCK",
]

SPEED_THRESHOLD = (
    0.5  # [km/h] Speed under which we do not classify the direction of vehicles
)
ZONE_DIRECTION = [1.5, -2.0, 0.0]

for frame_index, frame_dict in enumerate(osef.parser.parse(f"tcp://{SHIFT_IP}")):
    # Extract data from frame
    scan_frame = frame_dict["timestamped_data"]["scan_frame"]
    timestamp = frame_dict["timestamped_data"]["timestamp_microsecond"]
    tracked_objects = scan_frame["tracked_objects"]
    object_ids = tracked_objects.get("object_id_32_bits").tolist()
    zone_definitions = scan_frame["zones_def"]
    zone_bindings = scan_frame.get("zones_objects_binding_32_bits")
    class_names = tracked_objects["class_id_array"]["class_name"]

    wrong_way_vehicles: List[Any] = []

    for object_id, zone_index in zone_bindings:
        object_index = object_ids.index(object_id)
        object_class = class_names[object_index]
        zone_name = zone_definitions[zone_index]["zone"]["zone_name"]
        if object_class not in VEHICLE_NAMES or "wrongway" not in zone_name:
            continue

        # Vehicle is going the wrong way when the scalar product between
        # its speed and the zone direction is negative.
        speed_vector = tracked_objects["speed_vectors"].tolist()[object_index]
        speed = np.linalg.norm(speed_vector) * 3600 / 1000  # [km/h]
        direction = np.dot(speed_vector, ZONE_DIRECTION)

        if speed > SPEED_THRESHOLD and direction < 0:
            wrong_way_vehicles.append({object_id, object_class})

    if len(wrong_way_vehicles) > 0:
        time = datetime.datetime.fromtimestamp(timestamp).strftime("%d/%m/%Y %H:%M:%S")

        print(
            f"ALERT: At {time}, Vehicle(s) going in the wrong way direction: "
            f"{wrong_way_vehicles}"
        )
