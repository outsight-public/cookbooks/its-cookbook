"""Raise an alert when a certain amount of pedestrian crossed the crosswalk per hour (Recipe 3)."""
# Standard imports
from typing import Dict

# Third party imports
import osef

SHIFT_IP = "192.168.2.2"

pedestrian_on_crosswalk_ids: Dict[str, float] = {}
HOUR_IN_SECONDS = 1 * 60 * 60
PEDESTRIAN_THRESHOLD = 10
PEDESTRIAN_OBJECT = "PERSON"
CROSSWALK_ZONE = "crosswalk"

for frame_index, frame_dict in enumerate(osef.parser.parse(f"tcp://{SHIFT_IP}")):
    # Extract data from frame
    scan_frame = frame_dict["timestamped_data"]["scan_frame"]
    timestamp = frame_dict["timestamped_data"]["timestamp_microsecond"]
    tracked_objects = scan_frame["tracked_objects"]
    object_ids = tracked_objects.get("object_id_32_bits").tolist()
    zone_definitions = scan_frame["zones_def"]
    zone_bindings = scan_frame.get("zones_objects_binding_32_bits")
    class_names = tracked_objects["class_id_array"]["class_name"]

    # Count pedestrian in the crosswalk
    for object_id, zone_index in zone_bindings:
        object_index = object_ids.index(object_id)
        object_class = class_names[object_index]
        zone_name = zone_definitions[zone_index]["zone"]["zone_name"]
        if (
            object_class == PEDESTRIAN_OBJECT
            and CROSSWALK_ZONE in zone_name
            and object_id not in pedestrian_on_crosswalk_ids
        ):
            pedestrian_on_crosswalk_ids[object_id] = timestamp

    # Check number of pedestrian during the last hour
    pedestrians: int = 0
    pedestrians_to_remove = set()
    for ped_id, time in pedestrian_on_crosswalk_ids.items():
        if timestamp - time < HOUR_IN_SECONDS:
            pedestrians += 1
        else:
            pedestrians_to_remove.add(ped_id)

    if pedestrians > PEDESTRIAN_THRESHOLD:
        print(
            f"ALERT: During last hour, pedestrian threshold has been reached !"
            f"({pedestrians} > {PEDESTRIAN_THRESHOLD})"
        )

    for ped_id in pedestrians_to_remove:
        del pedestrian_on_crosswalk_ids[ped_id]
