# ITS Cookbook

LiDAR Cookbook for ITS Applications (Intelligent Transportation Systems).

It provides simple code samples (the recipes), allowing you to quickly achieve
precise and valuable results for many ITS use cases.

## Requirements

* Python3
* pip management system

# Use the code samples

We recommend to use a pip virtual environment.

```sh
virtualenv venv
source venv/bin/activate
pip3 install -r requirements.txt
```

In any example, we define a variable name `SHIFT_IP`, with a default value of `"192.168.2.2"`, you should define it according to your own hardware.

Then you can launch any examples with:

```sh
python3 Recipe_X/recipe.py

# Examples
python3 Recipe_1/pedestrian_alert.py
python3 Recipe_24/traffic_jam.py
```

# Recipes

| Number | Description |
| --- | --- |
| 1 | Crosswalk safety |
| 2 | People counting |
| 3 | Flow monitoring |
| 4 | Pedestrian safety |
| 5 | Pedestrian zone violation |
| 6 | Crosswalk violation |
| 7 | Vehicle near-miss |
| 8 | Pedestrian near-miss |
| 9 | Class-wise tolling |
| 10 | Vehicle dimension |
| 11 | Over-speed alert |
| 12 | Lane-wise over-speed |
| 13 | Class-wise over-speed |
| 14 | Over-speed statistics |
| 15 | Under-speed |
| 16 | Stop bar detection |
| 17 | Stop bar position |
| 18 | Illegal parking |
| 19 | Yellow box junction |
| 20 | Reserved lane violation |
| 21 | Illegal right turn |
| 22 | Wrong way |
| 23 | Lane change detection |
| 24 | Traffic jam starting |
| 25 | Traffic jam ending |