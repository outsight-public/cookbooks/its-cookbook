"""Raise an alert when a vehicle is close to a vehicle in a zone (Recipe #7)."""
# Standard imports
import itertools
from typing import List

# Third party imports
import osef
import numpy as np


SHIFT_IP = "192.168.2.2"

VEHICLE_NAMES = [
    "TWO_WHEELER",
    "CAR",
    "VAN",
    "TRUCK",
]

DANGER_DISTANCE = 5  # [meters]
SPEED_THRESHOLD = 10  # [km/h]

for frame_index, frame_dict in enumerate(osef.parser.parse(f"tcp://{SHIFT_IP}")):
    # Extract data from frame
    scan_frame = frame_dict["timestamped_data"]["scan_frame"]
    tracked_objects = scan_frame["tracked_objects"]
    object_ids = tracked_objects.get("object_id_32_bits").tolist()
    zone_definitions = scan_frame["zones_def"]
    zone_bindings = scan_frame.get("zones_objects_binding_32_bits")
    class_names = tracked_objects["class_id_array"]["class_name"]

    # Get all the vehicles in the danger zone
    vehicles_in_danger_zone: List[int] = []
    for object_id, zone_index in zone_bindings:
        object_index = object_ids.index(object_id)
        zone_name = zone_definitions[zone_index]["zone"]["zone_name"]
        if class_names[object_index] in VEHICLE_NAMES and "dangerzone" in zone_name:
            vehicles_in_danger_zone.append(object_index)

    if len(vehicles_in_danger_zone) < 2:
        continue

    for veh_a, veh_b in itertools.combinations(vehicles_in_danger_zone, 2):
        speed_vector_a = tracked_objects["speed_vectors"].tolist()[veh_a]
        speed_vector_b = tracked_objects["speed_vectors"].tolist()[veh_b]
        speed_a = np.linalg.norm(speed_vector_a) * 3600 / 1000
        speed_b = np.linalg.norm(speed_vector_b) * 3600 / 1000

        if speed_a < SPEED_THRESHOLD or speed_b < SPEED_THRESHOLD:
            continue

        pose_a = tracked_objects["pose_array"][veh_a]["translation"]
        pose_b = tracked_objects["pose_array"][veh_b]["translation"]
        distance = np.linalg.norm(pose_a - pose_b)

        if distance < DANGER_DISTANCE:
            print(
                f"ALERT: Vehicle {object_ids[veh_a]} is too close to vehicle {object_ids[veh_b]}"
            )
