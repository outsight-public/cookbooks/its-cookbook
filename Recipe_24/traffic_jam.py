"""Detect if more than two vehicles that were moving stop one after the other
for more than 30 seconds and then the average speed of the vehicles
is well under lowest speed. (Recipe #24)."""
# Standard imports
from typing import Dict, Set

# Third party imports
import numpy as np
import osef

SHIFT_IP = "192.168.2.2"

VEHICLE_NAMES = [
    "TWO_WHEELER",
    "CAR",
    "VAN",
    "TRUCK",
]
LOWER_SPEED = 5  # [km/h]
NORMAL_SPEED = 30  # [km/h]
STOP_TIME = 30  # seconds
moving_vehicles: Set[str] = set()
stopped_vehicles: Dict[int, Dict[str, int]] = {}

for frame_index, frame_dict in enumerate(osef.parser.parse(f"tcp://{SHIFT_IP}")):
    # Extract data from scan frame
    timestamp = frame_dict["timestamped_data"]["timestamp_microsecond"]
    scan_frame = frame_dict["timestamped_data"]["scan_frame"]
    tracked_objects = scan_frame["tracked_objects"]
    object_ids = tracked_objects.get("object_id_32_bits").tolist()
    class_names = tracked_objects["class_id_array"]["class_name"]

    # Check for moving/stopped vehicles
    detected_vehicles: Set[int] = set()
    for object_id, class_name, speed_vector in zip(
        object_ids, class_names, tracked_objects["speed_vectors"].tolist()
    ):
        if class_name not in VEHICLE_NAMES:
            continue
        detected_vehicles.add(object_id)
        speed = np.linalg.norm(speed_vector) * 3600 / 1000  # [km/h]
        if speed > NORMAL_SPEED:
            moving_vehicles.add(object_id)
            if object_id in stopped_vehicles:
                del stopped_vehicles[object_id]
        if speed < LOWER_SPEED and object_id in moving_vehicles:
            if object_id in stopped_vehicles:
                stopped_vehicles[object_id]["duration"] = (
                    timestamp - stopped_vehicles[object_id]["stop"]
                )
            else:
                stopped_vehicles[object_id] = {"stop": timestamp, "duration": 0}

    # Raise an alert in case of traffic jam.
    traffic_jam_counter = sum(
        stopped_vehicle["duration"] > STOP_TIME
        for stopped_vehicle in stopped_vehicles.values()
    )
    if traffic_jam_counter > 2:
        print("ALERT: Traffic jam ongoing")

    # Remove vehicles
    moving_vehicles = {
        veh_id for veh_id in moving_vehicles if int(veh_id) in detected_vehicles
    }

    stopped_vehicles = {
        veh_id: stopped_vehicles[veh_id]
        for veh_id in detected_vehicles
        if veh_id in stopped_vehicles
    }
