"""Raise an alert when a vehicle is traveling in a reserved lane (Recipe 20)"""
# Standard imports
import datetime
from typing import Set

# Third party imports
import osef

SHIFT_IP = "192.168.2.2"

VEHICLE_NAMES = ["TWO_WHEELER", "CAR", "VAN", "TRUCK"]
ZONE_NAME = "reserved_lane"

for frame_index, frame_dict in enumerate(osef.parser.parse(f"tcp://{SHIFT_IP}")):
    # Extract data from scan frame.
    timestamp = frame_dict["timestamped_data"]["timestamp_microsecond"]
    scan_frame = frame_dict["timestamped_data"]["scan_frame"]
    tracked_objects = scan_frame["tracked_objects"]
    object_ids = tracked_objects.get("object_id_32_bits").tolist()
    zone_definitions = scan_frame["zones_def"]
    zone_bindings = scan_frame.get("zones_objects_binding_32_bits")
    class_names = tracked_objects["class_id_array"]["class_name"]

    # Check vehicle in the reserved lane.
    vehicles_in_reserved_lanes: Set[int] = set()
    for object_id, zone_index in zone_bindings:
        object_index = object_ids.index(object_id)
        zone_name = zone_definitions[zone_index]["zone"]["zone_name"]

        if class_names[object_index] not in VEHICLE_NAMES or zone_name not in ZONE_NAME:
            continue
        vehicles_in_reserved_lanes.add(object_id)

    # Print alert
    if len(vehicles_in_reserved_lanes) > 0:
        print(f"ALERT: At {datetime.datetime.fromtimestamp(timestamp)}: ")
        print("Vehicle(s) traveling in the reserved lane: ")
        for veh_id in vehicles_in_reserved_lanes:
            object_index = object_ids.index(veh_id)
            print(f"* {class_names[object_index]} {veh_id}")
