"""Count the number of trucks which changed lane (Recipe 23)"""
# Standard imports
from typing import Dict, Set

# Third party imports
import osef

SHIFT_IP = "192.168.2.2"

VEHICLE_NAMES = [
    "TRUCK",
]

previous_trucks_in_lanes: Dict[str, Set[int]] = {  # zone_name: truck_set
    "first_lane": set(),
    "second_lane": set(),
    "third_lane": set(),
}

trucks_changing_counter: int = 0
trucks_in_lanes: Dict[str, Set[int]] = {}

for frame_index, frame_dict in enumerate(osef.parser.parse(f"tcp://{SHIFT_IP}")):
    # Extract data from frame
    scan_frame = frame_dict["timestamped_data"]["scan_frame"]
    tracked_objects = scan_frame["tracked_objects"]
    object_ids = tracked_objects.get("object_id_32_bits").tolist()
    zone_definitions = scan_frame["zones_def"]
    zone_bindings = scan_frame.get("zones_objects_binding_32_bits")
    class_names = tracked_objects["class_id_array"]["class_name"]

    current_trucks_in_lanes: Dict[str, Set[int]] = {
        lane_name: set() for lane_name in previous_trucks_in_lanes
    }

    for object_id, zone_index in zone_bindings:
        object_index = object_ids.index(object_id)
        object_class = class_names[object_index]
        zone_name = zone_definitions[zone_index]["zone"]["zone_name"]
        if object_class != "TRUCK" or zone_name not in previous_trucks_in_lanes:
            continue

        current_trucks_in_lanes[zone_name].add(object_id)

        # Check if truck was in another lane before.
        for name, trucks in trucks_in_lanes.items():
            if name != zone_name and object_id in trucks:
                trucks_changing_counter += 1

    if trucks_changing_counter > 0:
        print(f"INFO: {trucks_changing_counter} trucks have changed lanes.")

    # Update the trucks in lanes
    trucks_in_lanes = current_trucks_in_lanes
