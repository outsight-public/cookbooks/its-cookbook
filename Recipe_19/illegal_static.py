"""Raise an alert if a vehicle or pedestrian stays static in a no-stop zone (Recipe 19)"""
# Standard imports
from typing import Dict, Set

# Third party import
import numpy as np
import osef

SHIFT_IP = "192.168.2.2"
TIME_LIMIT = 1  # [minutes]

TRACKED_OBJECTS = ["TWO_WHEELER", "CAR", "VAN", "TRUCK", "PEDESTRIAN"]

ZONE_NAME = "nostop"
SPEED_LIMIT = 0.5  # [km/h]

static_objects: Dict[str, Dict[str, int]] = {}
nostop_zone_index: Set[int] = set()

for frame_index, frame_dict in enumerate(osef.parser.parse(f"tcp://{SHIFT_IP}")):
    # Extract data from scan frame.
    timestamp = frame_dict["timestamped_data"]["timestamp_microsecond"]
    scan_frame = frame_dict["timestamped_data"]["scan_frame"]
    tracked_objects = scan_frame["tracked_objects"]
    object_ids = tracked_objects.get("object_id_32_bits").tolist()
    zone_definitions = scan_frame["zones_def"]
    zone_bindings = scan_frame.get("zones_objects_binding_32_bits")
    class_names = tracked_objects["class_id_array"]["class_name"]

    # Check objects in the no-stop zone.
    objects_in_nostop = set()
    for object_id, zone_index in zone_bindings:
        object_index = object_ids.index(object_id)
        zone_name = zone_definitions[zone_index]["zone"]["zone_name"]

        if (
            class_names[object_index] not in TRACKED_OBJECTS
            or ZONE_NAME not in zone_name
        ):
            continue

        objects_in_nostop.add(object_id)
        speed_vector = tracked_objects["speed_vectors"][object_index]
        speed = np.linalg.norm(speed_vector) * 3600 / 1000  # [km/h]

        if speed > SPEED_LIMIT:
            continue

        # Update staying time.
        nostop_zone_index.add(zone_index)
        if object_id in static_objects:
            static_objects[object_id]["duration"] = (
                timestamp - static_objects[object_id]["arrival"]
            )
        else:
            static_objects[object_id] = {"arrival": timestamp, "duration": 0}

    # Remove objects who left the no-stop zone.
    objects_to_remove = set()
    for id_car, car in static_objects.items():
        if car["duration"] > TIME_LIMIT * 60:
            index = object_ids.index(id_car)
            print(
                f"ALERT: {class_names[index]} {id_car} stays static for more than {TIME_LIMIT} mins"
            )

        if id_car not in objects_in_nostop:
            objects_to_remove.add(id_car)

    for id_car in objects_to_remove:
        del static_objects[id_car]
