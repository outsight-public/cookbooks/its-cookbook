"""Raise an alert when a Vehicle is not stopping at the Stop bar (Recipe #26)."""
# Standard imports
from typing import Dict, Any, Set

# Third party imports
import numpy as np
import osef

SHIFT_IP = "192.168.2.2"

VEHICLE_NAMES = [
    "TWO_WHEELER",
    "CAR",
    "VAN",
    "TRUCK",
]

SPEED_LIMIT = 0.2  # [km/h]
STOP_BAR_ZONE = "stop_bar"
stopping_vehicles: Dict[int, Dict[str, Any]] = {}

for frame_index, frame_dict in enumerate(osef.parser.parse(f"tcp://{SHIFT_IP}")):
    # Extract data from scan frame
    scan_frame = frame_dict["timestamped_data"]["scan_frame"]
    tracked_objects = scan_frame["tracked_objects"]
    object_ids = tracked_objects.get("object_id_32_bits").tolist()
    class_names = tracked_objects["class_id_array"]["class_name"]
    zone_definitions = scan_frame["zones_def"]
    zone_bindings = scan_frame.get("zones_objects_binding_32_bits")

    # Check if a vehicle didn't stop at the stop bar
    vehicles_in_zone: Set[int] = set()
    for object_id, zone_index in zone_bindings:
        object_index = object_ids.index(object_id)
        if class_names[object_index] not in VEHICLE_NAMES:
            continue

        zone_name = zone_definitions[zone_index]["zone"]["zone_name"]
        if STOP_BAR_ZONE not in zone_name:
            continue

        vehicles_in_zone.add(object_id)
        speed_vector = tracked_objects["speed_vectors"].tolist()[object_index]
        speed = np.linalg.norm(speed_vector) * 3600 / 1000  # [km/h]

        if object_id not in stopping_vehicles:
            stopping_vehicles[object_id] = {
                "stopped": False,
                "type": class_names[object_index],
            }
        if speed < SPEED_LIMIT:
            stopping_vehicles[object_id]["stopped"] = True

    # When vehicle is leaving the stop bar zone, check if it stopped
    vehicle_to_remove: Set[int] = set()
    for veh_id, values in stopping_vehicles.items():
        if veh_id not in vehicles_in_zone:
            if not values["stopped"]:
                vehicle_type = values["type"]
                print(f"ALERT: {vehicle_type} {veh_id} did not stop at the stop bar.")
            vehicle_to_remove.add(veh_id)

    for veh_id in vehicle_to_remove:
        del stopping_vehicles[veh_id]
