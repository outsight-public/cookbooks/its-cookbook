"""Raise an alert when a Vehicle is stopping at the Stop bar (Recipe #28)."""
# Standard imports
from typing import Set

# Third party imports
import numpy as np
import osef

SHIFT_IP = "192.168.2.2"

VEHICLE_NAMES = ["TWO_WHEELER", "CAR", "VAN", "TRUCK"]

SPEED_LIMIT = 0.2  # [km/h]
stopping_vehicles: Set[int] = set()

for frame_index, frame_dict in enumerate(osef.parser.parse(f"tcp://{SHIFT_IP}")):
    # Extract data from scan frame
    scan_frame = frame_dict["timestamped_data"]["scan_frame"]
    tracked_objects = scan_frame["tracked_objects"]
    object_ids = tracked_objects.get("object_id_32_bits").tolist()
    class_names = tracked_objects["class_id_array"]["class_name"]
    zone_definitions = scan_frame["zones_def"]
    zone_bindings = scan_frame.get("zones_objects_binding_32_bits")

    # Check if a vehicle is stopped in the defined zone
    for object_id, zone_index in zone_bindings:
        object_index = object_ids.index(object_id)
        if class_names[object_index] not in VEHICLE_NAMES:
            continue

        zone_name = zone_definitions[zone_index]["zone"]["zone_name"]
        speed_vector = tracked_objects["speed_vectors"].tolist()[object_index]
        speed = np.linalg.norm(speed_vector) * 3600 / 1000  # [km/h]

        if "stopbar" in zone_name and speed < SPEED_LIMIT:
            print(f"ALARM: Vehicle {object_index} is stopping at the stop bar")
            stopping_vehicles.add(object_index)

print(f"A total of {len(stopping_vehicles)} vehicles have stopped at the stop bar")
