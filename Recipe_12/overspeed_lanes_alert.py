"""Raise an alert with position and speed of the vehicle, when a Vehicle over speeds(Recipe #12)."""
# Third party imports
import osef
import numpy as np


SHIFT_IP = "192.168.2.2"

VEHICLE_NAMES = ["TWO_WHEELER", "CAR", "VAN", "TRUCK"]

ZONE_LEFT = "leftlane"
ZONE_RIGHT = "rightlane"
LANE_SPEEDS = {ZONE_LEFT: 45, ZONE_RIGHT: 30}  # [km/h]


for frame_index, frame_dict in enumerate(osef.parser.parse(f"tcp://{SHIFT_IP}")):
    # extract data from scan frame
    scan_frame = frame_dict["timestamped_data"]["scan_frame"]
    tracked_objects = scan_frame["tracked_objects"]
    object_ids = tracked_objects.get("object_id_32_bits").tolist()
    zone_definitions = scan_frame["zones_def"]
    zone_bindings = scan_frame.get("zones_objects_binding_32_bits")
    class_names = tracked_objects["class_id_array"]["class_name"]

    for object_id, zone_index in zone_bindings:
        object_index = object_ids.index(object_id)
        zone_name = zone_definitions[zone_index]["zone"]["zone_name"]

        if class_names[object_index] not in VEHICLE_NAMES:
            continue

        lane_name: str = (
            ZONE_RIGHT
            if ZONE_RIGHT in zone_name
            else ZONE_LEFT
            if ZONE_LEFT in zone_name
            else ""
        )

        if not lane_name:
            continue

        speed_limit = LANE_SPEEDS[lane_name]
        position = tracked_objects["pose_array"][object_index]["translation"]
        speed_vector = tracked_objects["speed_vectors"].tolist()[object_index]
        speed = np.linalg.norm(speed_vector) * 3600 / 1000  # [km/h]
        if speed > speed_limit:
            vehicle_id = object_ids[object_index]
            print(
                f"ALARM: Vehicle {vehicle_id} caught speeding on {lane_name} "
                f"at position {position[:2]} "
                f"({speed_limit} km/h < {round(speed, 2)} km/h)"
            )
