"""Raise an alert if vehicles travelling at normal speed in a traffic Jam activated zone (R25)"""
# Standard imports
from typing import Set

# Third party imports
import numpy as np
import osef

SHIFT_IP = "192.168.2.2"

VEHICLE_NAMES = [
    "TWO_WHEELER",
    "CAR",
    "VAN",
    "TRUCK",
]
NORMAL_SPEED = 30  # [km/h]
ZONE_NAME = "traffic_jam"

for frame_index, frame_dict in enumerate(osef.parser.parse(f"tcp://{SHIFT_IP}")):
    # Extract data from scan frame
    scan_frame = frame_dict["timestamped_data"]["scan_frame"]
    tracked_objects = scan_frame["tracked_objects"]
    object_ids = tracked_objects.get("object_id_32_bits").tolist()
    class_names = tracked_objects["class_id_array"]["class_name"]
    zone_definitions = scan_frame["zones_def"]
    zone_bindings = scan_frame.get("zones_objects_binding_32_bits")

    # Check if vehicles are traveling at a normal speed in the traffic jam zone.
    vehicles_normal_speed: Set[int] = set()
    for object_id, zone_index in zone_bindings:
        object_index = object_ids.index(object_id)
        if class_names[object_index] not in VEHICLE_NAMES:
            continue

        zone_name = zone_definitions[zone_index]["zone"]["zone_name"]
        if zone_name not in ZONE_NAME:
            continue

        speed_vector = tracked_objects["speed_vectors"].tolist()[object_index]
        speed = np.linalg.norm(speed_vector) * 3600 / 1000  # [km/h]
        if speed > NORMAL_SPEED:
            vehicles_normal_speed.add(object_id)

    # Raise an alert if needed.
    if len(vehicles_normal_speed) > 0:
        print(
            f"ALERT: Vehicles {vehicles_normal_speed} traveling at normal speed "
            f"in the Traffic Jam zone (> {NORMAL_SPEED}km/h)"
        )
