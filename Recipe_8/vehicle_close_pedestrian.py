"""Raise an alert when a vehicle is close to a pedestrian in a crosswalk (Recipe #8)."""
import numpy as np
import osef

SHIFT_IP = "192.168.2.2"

VEHICLE_NAMES = [
    "TWO_WHEELER",
    "CAR",
    "VAN",
    "TRUCK",
]

DANGER_DISTANCE = 5  # [meters]
SPEED_THRESHOLD = 10  # [km/h]

for frame_index, frame_dict in enumerate(osef.parser.parse(f"tcp://{SHIFT_IP}")):
    # Extract data from frame
    scan_frame = frame_dict["timestamped_data"]["scan_frame"]
    tracked_objects = scan_frame["tracked_objects"]
    object_ids = tracked_objects.get("object_id_32_bits").tolist()
    zone_definitions = scan_frame["zones_def"]
    zone_bindings = scan_frame.get("zones_objects_binding_32_bits")
    class_names = tracked_objects["class_id_array"]["class_name"]

    # Check if vehicle is close to a pedestrian in a crosswalk zone
    for object_id, zone_index in zone_bindings:
        object_index = object_ids.index(object_id)
        zone_name = zone_definitions[zone_index]["zone"]["zone_name"]
        if class_names[object_index] != "PERSON" or "crosswalk" not in zone_name:
            continue

        for veh_index, veh_pose, class_name, speed_vector in zip(
            object_ids,
            tracked_objects["pose_array"],
            class_names,
            tracked_objects["speed_vectors"].tolist(),
        ):
            if class_name not in VEHICLE_NAMES:
                continue

            speed = np.linalg.norm(speed_vector) * 3600 / 1000
            if speed < SPEED_THRESHOLD:
                continue

            ped_pose = tracked_objects["pose_array"][object_index]
            distance = np.linalg.norm(veh_pose["translation"] - ped_pose["translation"])
            if distance < DANGER_DISTANCE:
                print(
                    f"ALERT: Vehicle {object_ids[veh_index]} "
                    f"is too close to pedestrian in crosswalk"
                )
